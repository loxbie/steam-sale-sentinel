# Steam Sale Sentinel

Steam Sale Sentinel is a web application built with Flask that allows users to check for sales on their Steam wishlist. It fetches data from Steam's API to retrieve wishlist information and checks for discounted games. Users can input their Steam ID and select their preferred currency (currently supporting €) to see which games on their wishlist are currently on sale.

## Features

- Fetches Steam wishlist data using Steam's API.
- Checks for discounted games on the wishlist.
- Displays discounted games sorted by discount percentage and priority.
- Provides direct links to purchase discounted games on Steam.

## Setup

To run this project locally:

1. Clone this repository.
   ```bash
   git clone <repository_url>
   cd steam-sale-sentinel
   ```

2. Install dependencies.
   ```bash
   pip install -r requirements.txt
   ```
3. Run the Flask application:
   ```bash
   python app.py
   ```

5. Open your web browser and navigate to `https://localhost:5000`.

## Technologies Used

- Python
- Flask
- HTML/CSS
- JavaScript (Vanilla JS)
- GitLab for version control and CI/CD (optional)

## Notes

- This project currently supports fetching wishlist data from Steam and checking for discounts in Euros (€).
- Ensure your Steam profile and wishlist are public to retrieve accurate data.

Feel free to contribute to this project by submitting pull requests or reporting issues.

