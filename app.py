import requests
from flask import Flask, render_template, request, jsonify
from flask_caching import Cache

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

API_KEY = 'YOUR_STEAM_API_KEY'
CURRENCY = "€"

def get_wishlist(steam_id):
    url = f'https://store.steampowered.com/wishlist/profiles/{steam_id}/wishlistdata/'
    response = requests.get(url)
    
    if response.status_code == 200:
        return response.json()
    else:
        return None

@cache.memoize(timeout=300)  # Cache results for 300 seconds (5 minutes)
def check_sales_cached(steam_id):
    wishlist = get_wishlist(steam_id)
    if wishlist:
        sales = []
        for appid, game_info in wishlist.items():
            price_info = game_info.get('subs', [])
            for sub in price_info:
                if 'discount_pct' in sub and sub['discount_pct'] is not None:
                    sales.append({
                        'id' : appid,
                        'name': game_info['name'],
                        'discount': sub['discount_pct'],
                        'priority': game_info.get('priority', 99),  # Default priority if not provided
                        'price': float(sub['price']) / (100 - float(sub['discount_pct'])),
                        'discounted_price': float(sub['price']) / 100,
                        'link': f'https://store.steampowered.com/app/{appid}'
                    })

        # Sort sales by discount (descending) and then by priority (ascending)
        sales.sort(key=lambda x: (-x['discount'], x['priority']))
        return sales
    else:
        return []

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/check_sales', methods=['POST'])
def get_sales():
    steam_id = request.form.get('steam_id')
    currency = request.form.get('currency', '€')

    try:
        sales = check_sales_cached(steam_id)
        for sale in sales:
            sale['currency'] = currency
        return jsonify(sales)
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True, ssl_context='adhoc')
