import requests

API_KEY = 'YOUR_STEAM_API_KEY'
STEAM_ID = '76561198070768228'
CURRENCY = "€"

def get_wishlist(steam_id):
    url = f'https://store.steampowered.com/wishlist/profiles/{steam_id}/wishlistdata/'
    response = requests.get(url)
    
    if response.status_code == 200:
        return response.json()
    else:
        response.raise_for_status()

def check_sales(wishlist):
    sales = []
    for appid, game_info in wishlist.items():
        price_info = game_info.get('subs', [])
        for sub in price_info:
            if 'discount_pct' in sub and sub['discount_pct'] is not None:
                sales.append({
                    'id' : appid,
                    'name': game_info['name'],
                    'discount': sub['discount_pct'],
                    'priority': game_info.get('priority'),
                    'price': float(sub['price']) / (100 - float(sub['discount_pct'])),
                    'discounted_price': (float(sub['price']) / 100)
                })
    return sales

if __name__ == "__main__":
    try:
        wishlist = get_wishlist(STEAM_ID)
        sales = check_sales(wishlist)
        if sales:
            sales.sort(key=lambda x: (x['discount'],x['priority']), reverse=True)
            for sale in sales:
                print(f"{sale['name']} - {sale['discount']}%")
                print(f"\tIt is placed {sale['priority']}. on their wishlist. Was {sale['price']:.2f}{CURRENCY}. now only {sale['discounted_price']:.2f}{CURRENCY}.")
                print(f"\tBuy here: https://store.steampowered.com/app/{sale['id']}")
        else:
            print("No sales currently.")
    except Exception as e:
        print(f"An error occurred: {e}")
